webpackJsonp([1],{

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_Util__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * ionic cordova plugin add cordova-plugin-file
 * npm install --save @ionic-native/file
 *
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
/** Consulta FIPE https://systems.eti.br/artigo/api-tabela-fipe */
var DetailsPage = /** @class */ (function () {
    function DetailsPage(navCtrl, navParams, http, _platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this._platform = _platform;
        this.API_DET_URL = 'http://fipeapi.appspot.com/api/1/carros/veiculos/';
        if (this._platform.is("cordova")) {
            console.log("device");
        }
        this.id = navParams.get('id');
        this.label = navParams.get('label');
        this.imageURL = navParams.get('imageURL');
        this.getWS(this.id);
        this.initializeItems();
    }
    DetailsPage.prototype.filterList = function (e) {
        var search = e.target.value;
        this.initializeItems();
        if (search && search.trim() != '') {
            this.carList = this.carList.filter(function (item) {
                return (item.name.toLowerCase().indexOf(search.toLowerCase()) > -1);
            });
        }
    };
    DetailsPage.prototype.initializeItems = function () {
        this.carList = this.root;
        //console.log(this.file.dataDirectory);
        //this.file.checkDir('../../../src/assets/json', 'years').then(_ => console.log('Directory exists')).catch(err => console.log('Directory doesn\'t exist'));
    };
    DetailsPage.prototype.getWS = function (id) {
        var _this = this;
        //console.log("Carregando");
        this.http.get(this.API_DET_URL + id + ".json")
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.root = data;
        }, function (err) {
            console.log("ERROR");
        }, function () {
            _this.initializeItems();
            _this.getYears(_this.root, id);
            console.log(_this.root);
        });
    };
    DetailsPage.prototype.getYears = function (array, id_marca) {
        var _this = this;
        var _loop_1 = function (i) {
            var url = window.location.href + "assets/json/years/" + id_marca + "_" + array[i]['id'] + ".json";
            this_1.http.get(url)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                array[i]['years'] = data;
                _this.getDetails(array[i], id_marca, array[i]['id']);
            });
        };
        var this_1 = this;
        for (var i = 0; i < array.length; i++) {
            _loop_1(i);
        }
    };
    DetailsPage.prototype.getDetails = function (arrayYears, id_marca, id_modelo) {
        arrayYears['menor_ano'] = "";
        arrayYears['maior_ano'] = "";
        arrayYears['menor_valor'] = "";
        arrayYears['maior_valor'] = "";
        var _loop_2 = function (j) {
            var url = window.location.href + "assets/json/details/" + id_marca + "_" + id_modelo + "_" + arrayYears['years'][j]['Value'] + ".json";
            this_2.http.get(url)
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                arrayYears['years'][j]['details'] = data;
                if (data != null && data != undefined && data.Valor != undefined && data.Valor != null) {
                    var valor = __WEBPACK_IMPORTED_MODULE_3__app_Util__["a" /* Util */].sanitize(data.Valor);
                    var menorV = __WEBPACK_IMPORTED_MODULE_3__app_Util__["a" /* Util */].sanitize(arrayYears['maior_valor']);
                    var maiorV = __WEBPACK_IMPORTED_MODULE_3__app_Util__["a" /* Util */].sanitize(arrayYears['menor_valor']);
                    if (data.AnoModelo < arrayYears['menor_ano'] || arrayYears['menor_ano'] == "")
                        arrayYears['menor_ano'] = data.AnoModelo;
                    if (data.AnoModelo > arrayYears['maior_ano'] || arrayYears['maior_ano'] == "")
                        arrayYears['maior_ano'] = data.AnoModelo;
                    if (valor < menorV || menorV == "")
                        arrayYears['menor_valor'] = data.Valor;
                    if (valor > maiorV || maiorV == "")
                        arrayYears['maior_valor'] = data.Valor;
                }
            }, function (err) {
                console.log("ERROR");
            }, function () {
                if (j == arrayYears['years'].length - 1) {
                    console.log("carregado!!");
                }
            });
        };
        var this_2 = this;
        for (var j = 0; j < arrayYears['years'].length; j++) {
            _loop_2(j);
        }
    };
    DetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-details',template:/*ion-inline-start:"/home/nairan/Área de Trabalho/application/src/pages/details/details.html"*/'<!--\n  Generated template for the DetailsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title id="title">{{label}}</ion-title>\n    <img id="logo" src=\'{{imageURL}}\'>\n  </ion-navbar>\n  <ion-toolbar>\n    <ion-searchbar (ionInput)="filterList($event)" placeholder="Pesquisar..."></ion-searchbar>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content padding>\n    \n    <ion-list >\n        <ion-item *ngFor="let data of carList">\n              <div id="label">{{data.name}}</div>\n              <div id="details">\n                  <ion-icon class=\'icon-down\' ios="ios-arrow-round-down" md="md-arrow-round-down">\n                        <span>{{data.menor_valor}}</span>\n                  </ion-icon>\n                  <ion-icon class=\'icon-up\' ios="ios-arrow-round-up" md="md-arrow-round-up">\n                      <span>{{data.maior_valor}}</span>\n                  </ion-icon>\n            </div>\n            <button ion-button clear item-end>Detalhes</button>\n        </ion-item>\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/nairan/Área de Trabalho/application/src/pages/details/details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* Platform */]])
    ], DetailsPage);
    return DetailsPage;
}());

//# sourceMappingURL=details.js.map

/***/ }),

/***/ 115:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 115;

/***/ }),

/***/ 156:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/details/details.module": [
		283,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 156;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Util; });
/**
 * Default
 *
 * @author Nairan Omura<nairanomura@hotmail.com>
 * @since 28/10/2018
 */
var Util = /** @class */ (function () {
    function Util() {
    }
    /**ionic ser
     * Método para organização de JSON por attributos
     * Ex.: let array = [{nome: ana, idade: 18}, ... ];
     *
     *      result = Util.bucketSort(array, 'nome');
     *
     * @author Nairan Omura<nairanomura@hotmail.com>
     * @since 28/10/2018
     *
     * @param array Objeto JSON a ser organizado
     * @param attribute Atributo índice da organização
     *
     * @return JSON ordenado conforme atributo recebido
     */
    Util.bucketSort = function (array, attribute) {
        var bucket = new Array();
        var _loop_1 = function (i) {
            var first = array[i][attribute]
                .toLowerCase()
                .charCodeAt();
            var check = false;
            bucket.forEach(function (ele, j) {
                if (j == first) {
                    bucket[first].unshift(array[i]);
                    check = true;
                    return false;
                }
            });
            if (!check)
                bucket[first] = [array[i]];
        };
        /** BucketSort */
        for (var i = 0; i < array.length; i++) {
            _loop_1(i);
        }
        /** SelectionSort*/
        bucket.forEach(function (ele, i) {
            ele.forEach(function (item, j) {
                var menor = ele[j];
                ele.forEach(function (subitem, k) {
                    var valida = false, index = 0;
                    if (menor[attribute] == subitem[attribute])
                        return true;
                    while (!valida && menor[attribute].length > index++) {
                        var first = (menor[attribute]).charCodeAt(index);
                        var second = (subitem[attribute]).charCodeAt(index);
                        if (first > second || isNaN(second)) {
                            ele[j] = subitem;
                            ele[k] = menor;
                            menor = subitem;
                            valida = true;
                        }
                        else if (first < second) {
                            valida = true;
                        }
                    }
                });
            });
        });
        var ArrayResult = new Array();
        bucket.forEach(function (item, i) {
            for (var j = item.length - 1; j >= 0; j--) {
                ArrayResult.push(item[j]);
            }
        });
        return ArrayResult;
    };
    Util.fileExists = function (url) {
        if (url) {
            var req = new XMLHttpRequest();
            req.open('GET', url, false);
            req.send();
            return req.status == 200;
        }
        else {
            return false;
        }
    };
    Util.sanitize = function (string) {
        if (string == undefined)
            return undefined;
        if (string == "")
            return "";
        string = string.replace("R$", "");
        string = string.replace(" ", "");
        string = string.replace(".", "");
        string = string.replace(",", "");
        return string;
    };
    return Util;
}());

//# sourceMappingURL=Util.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__about_about__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_contact__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(203);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__about_about__["a" /* AboutPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__contact_contact__["a" /* ContactPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/nairan/Área de Trabalho/application/src/pages/tabs/tabs.html"*/'<ion-tabs>\n  <ion-tab [root]="tab1Root" tabTitle="Home" tabIcon="home"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="About" tabIcon="information-circle"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="Contact" tabIcon="contacts"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"/home/nairan/Área de Trabalho/application/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(102);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl, sqlite, sqliteObj) {
        this.navCtrl = navCtrl;
        this.sqlite = sqlite;
        this.sqliteObj = sqliteObj;
    }
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"/home/nairan/Área de Trabalho/application/src/pages/about/about.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      About\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/home/nairan/Área de Trabalho/application/src/pages/about/about.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["b" /* SQLiteObject */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContactPage = /** @class */ (function () {
    function ContactPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"/home/nairan/Área de Trabalho/application/src/pages/contact/contact.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Contact\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-list-header>Follow us on Twitter</ion-list-header>\n    <ion-item>\n      <ion-icon name="ionic" item-start></ion-icon>\n      @ionicframework\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/nairan/Área de Trabalho/application/src/pages/contact/contact.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_Util__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__details_details__ = __webpack_require__(103);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







//import { HttpClient } from '@angular/common/http';
var HomePage = /** @class */ (function () {
    //private API_URL = 'https://parallelum.com.br/fipe/api/v1/carros/marcas';
    //private API_DET_URL = 'http://fipeapi.appspot.com/api/1/carros/veiculos';
    //private API_DET_URL = 'https://parallelum.com.br/fipe/api/v1/carros/marcas/';
    function HomePage(navCtrl, http) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.API_URL = 'http://fipeapi.appspot.com/api/1/carros/marcas.json';
        this.rootList = this.getWS();
        this.initializeItems();
    }
    HomePage.prototype.initializeItems = function () {
        this.carList = this.rootList;
    };
    HomePage.prototype.filterList = function (e) {
        var search = e.target.value;
        this.initializeItems();
        if (search && search.trim() != '') {
            this.carList = this.carList.filter(function (item) {
                return (item.name.toLowerCase().indexOf(search.toLowerCase()) > -1);
            });
        }
    };
    HomePage.prototype.getWS = function () {
        var result = [];
        this.http.get(this.API_URL).subscribe(function (data) {
            try {
                if (data) {
                    var array = __WEBPACK_IMPORTED_MODULE_0__app_Util__["a" /* Util */].bucketSort(JSON.parse(data['_body']), 'name');
                    array.forEach(function (item) {
                        var imageURL = "../../../assets/imgs/" + (item.name.replace(' ', '-')).toLowerCase() + ".png";
                        var errorURL = "../../../assets/imgs/error.jpg";
                        item.name = item.name.toUpperCase();
                        item.imageURL = __WEBPACK_IMPORTED_MODULE_0__app_Util__["a" /* Util */].fileExists(imageURL) ? imageURL : errorURL;
                        result.push(item);
                    });
                }
            }
            catch (e) {
                console.log(e);
            }
        }, function (error) {
            console.log("Erro ao consultar WebService");
            return false;
        }, function () {
            console.log("sucesso");
        });
        return result;
    };
    HomePage.prototype.detailsPage = function (id, label, imageURL) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__details_details__["a" /* DetailsPage */], { "id": id, "label": label, "imageURL": imageURL });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/nairan/Área de Trabalho/application/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n  <ion-toolbar>\n    <ion-searchbar (ionInput)="filterList($event)" placeholder="Pesquisar..."></ion-searchbar>\n  </ion-toolbar>\n</ion-header>\n<ion-content padding>\n  <ion-list id="carList">\n      <ion-item *ngFor="let data of carList">\n          <ion-thumbnail item-start>\n              <img src=\'{{data.imageURL}}\'>\n          </ion-thumbnail>\n          <h2>{{data.name}}</h2>\n          <p> • <!-- media de preços/ preço min e preço max--></p>\n          <button ion-button clear item-end (click)="detailsPage(data.id, data.name, data.imageURL);">Ver</button>\n      </ion-item>\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"/home/nairan/Área de Trabalho/application/src/pages/home/home.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatabaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__ = __webpack_require__(102);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var DatabaseProvider = /** @class */ (function () {
    function DatabaseProvider(http, sqlite) {
        this.http = http;
        this.sqlite = sqlite;
    }
    DatabaseProvider.prototype.getDB = function () {
        return this.sqlite.create({
            name: 'radios.db',
            location: 'default'
        });
    };
    DatabaseProvider.prototype.createDatabase = function () {
        var _this = this;
        return this.getDB()
            .then(function (db) {
            _this.createTables(db);
            _this.insertDefaultItems(db);
        })
            .catch(function (e) { return console.log(e); });
    };
    DatabaseProvider.prototype.createTables = function (db) {
        db.sqlBatch([
            ['CREATE TABLE IF NOT EXISTS radios (id integer primary key AUTOINCREMENT NOT NULL, name TEXT, url TEXT, thumb TEXT)']
        ])
            .then(function () { return console.log('tabelas criadas'); })
            .catch(function (e) { return console.error('Erro ao criar as tabelas', e); });
    };
    DatabaseProvider.prototype.insertDefaultItems = function (db) {
        db.executeSql('select COUNT(id) as qtd from radios', [])
            .then(function (data) {
            if (data.rows.item(0).qtd == 0) {
                db.sqlBatch([
                    ['insert into radios (name,url,thumb) values (?,?,?)', ['radio 1', 'url de teste 1', 'thumb 1']],
                    ['insert into radios (name,url,thumb) values (?,?,?)', ['radio 2', 'url de teste 2', 'thumb 2']]
                ])
                    .then(function () { return console.log('Dados default incluídos com sucesso!'); })
                    .catch(function (e) { return console.error('Erro ao incluir os dados default', e); });
            }
        })
            .catch(function (e) { return console.error('Erro ao consultar a qtd de radios', e); });
    };
    DatabaseProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_sqlite__["a" /* SQLite */]])
    ], DatabaseProvider);
    return DatabaseProvider;
}());

//# sourceMappingURL=database.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(226);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 226:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_details_details__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_about_about__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_contact_contact__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_home_home__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_tabs_tabs__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_sqlite__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_status_bar__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_splash_screen__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_database_database__ = __webpack_require__(204);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_0__pages_details_details__["a" /* DetailsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/details/details.module#DetailsPageModule', name: 'DetailsPage', segment: 'details', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* HttpModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_0__pages_details_details__["a" /* DetailsPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_sqlite__["a" /* SQLite */],
                __WEBPACK_IMPORTED_MODULE_13__providers_database_database__["a" /* DatabaseProvider */],
                { provide: __WEBPACK_IMPORTED_MODULE_2__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_13__providers_database_database__["a" /* DatabaseProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_database_database__ = __webpack_require__(204);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, dbProvider) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            dbProvider.createDatabase();
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/nairan/Área de Trabalho/application/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/home/nairan/Área de Trabalho/application/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_5__providers_database_database__["a" /* DatabaseProvider */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[205]);
//# sourceMappingURL=main.js.map