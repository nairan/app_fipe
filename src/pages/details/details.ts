import { Component} from '@angular/core';
import { Http } from '@angular/http';
import { IonicPage, NavController, NavParams, Platform, ModalController, LoadingController } from 'ionic-angular';
import { Util } from './../../app/Util';
import { YearsPage } from '../years/years';
import { ModelsPage } from '../models/models';

 /** Consulta FIPE https://systems.eti.br/artigo/api-tabela-fipe */

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})

export class DetailsPage {
  public id;
  public imageURL;
  public label;
  public carList;
  public yearList;
  public marca = "modelo";
  private URL_BASE = '/fipe';
  private rootModels;
  private rootYears;
  private API_DET_URL = 'http://fipeapi.appspot.com/api/1/carros/veiculos/';
  private loader/*: Loading*/;

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http,
    private _platform: Platform, private loadingCtrl: LoadingController) {

      if (this._platform.is("cordova")) {
        console.log("device");
      }

      this.loader = this.loadingCtrl.create({
	 spinner: "crescent",
	 content: "Consultando...",
	 //duration: 3000
      });

      this.id = navParams.get('id');
      this.label = navParams.get('label');
      this.imageURL = navParams.get('imageURL');

      this.getWS(this.id);
      this.initializeItems();
	
      this.getCarForYears(this.id);
  }

  public yearsPage(id_modelo, name) {
	this.navCtrl.push(YearsPage, {"id_marca": this.id, "id_modelo": id_modelo, "name": name});
  }

  public modelsPage(id_year) {
	this.navCtrl.push(ModelsPage, {"id_marca": this.id, "id_year": id_year});
  }

  public filterList(e) {
    const search = e.target.value;
    this.initializeItems();

    if (search && search.trim() != '') {
      this.carList = this.carList.filter((item) => {
        return (item.name.toLowerCase().indexOf(search.toLowerCase()) > -1);
      });
    }
  }

  private initializeItems() {
    this.carList = this.rootModels;
    this.yearList = this.rootYears;
  }

  private getWS(id) {
    this.loader.present();
    this.http.get(this.API_DET_URL+id+".json")
      .map((res) => res.json())
      .subscribe((data) => { 
          this.rootModels = data;
      }, err => {
        console.log("ERROR");         
      },() => {
        this.initializeItems();
        this.loader.dismiss();

	//getCarForYears(id)
      });
  }

  private getCarForYears(id) {
    //console.log("Carregando");
    this.http.get(this.URL_BASE+'?veiculo=carro/'+id+'/anos')
      .map((res) => res.json())
      .subscribe((data) => { 
          this.rootYears = data;
      }, err => {
        console.log("ERROR");         
      },() => {
        this.initializeItems();
	console.log(this.rootYears);
      });
  }

  getYears(array, id_marca) {
    for (let i=0; i<array.length; i++) {
      let url = window.location.href +"assets/json/years/"+id_marca+"_"+array[i]['id']+".json";
            this.http.get(url)
              .map((res) => res.json())
              .subscribe((data) => { 
                  array[i]['years'] = data;
                  this.getDetails(array[i], id_marca, array[i]['id']);
              });
    }
  }

  getDetails(arrayYears, id_marca, id_modelo) {

    arrayYears['menor_ano'] = "";
    arrayYears['maior_ano'] = "";

    arrayYears['menor_valor'] = "";
    arrayYears['maior_valor'] = "";

    for(let j=0; j<arrayYears['years'].length; j++) {
      let url = window.location.href +"assets/json/details/"+id_marca+"_"+id_modelo+"_"+arrayYears['years'][j]['Value']+".json";    
      this.http.get(url)
        .map((res) => res.json())
        .subscribe((data) => { 
            arrayYears['years'][j]['details'] = data;

          if (data!=null && data!=undefined && data.Valor!=undefined && data.Valor != null) {
            let valor =  Util.sanitize(data.Valor);
            let menorV = Util.sanitize(arrayYears['maior_valor']);
            let maiorV = Util.sanitize(arrayYears['menor_valor']);
		
            if (data.AnoModelo<arrayYears['menor_ano'] || arrayYears['menor_ano'] == "") arrayYears['menor_ano'] = data.AnoModelo;
            
            if (data.AnoModelo>arrayYears['maior_ano'] || arrayYears['maior_ano'] == "") arrayYears['maior_ano'] = data.AnoModelo;			

            if (valor<menorV || menorV == "") arrayYears['menor_valor'] = data.Valor;
            
            if (valor>maiorV || maiorV == "") arrayYears['maior_valor'] = data.Valor;			
        }
      }, err => {
        console.log("ERROR");         
      },() => {
        //console.log(data);
        if (j == arrayYears['years'].length-1) {
          console.log("carregado!!");
        }
      });
        
    }
  }
}
