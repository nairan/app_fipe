import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-models',
  templateUrl: 'models.html',
})
export class ModelsPage {
  private id_marca;
  public id_year;
  private API_DET_URL = '/fipe';
  private API_DET_URL_OTHER = 'http://fipeapi.appspot.com/api/1/carros/veiculo/';
  private rootModels;
  public modelsList;
  private loader /*: Loading*/;

//?veiculo=carro/59/2015-1

  constructor(public http: Http, public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController) {
      this.id_marca = navParams.get('id_marca');
      this.id_year = navParams.get('id_year');

      this.loader = this.loadingCtrl.create({
	 spinner: "crescent",
	 content: "Consultando...",
	 //duration: 3000
      });

      this.getWS();
  }

  private getWS() {
    this.loader.present();
    this.http.get(this.API_DET_URL+"?veiculo=carro/"+this.id_marca+"/"+this.id_year+"/")
      .map((res) => res.json())
      .subscribe((data) => { 
          this.rootModels = data;
          
	  for(let i=0;i<data.length;i++) {
               this.getPrices(this.rootModels[i], data[i].Value)
          }
      }, err => {
        console.log("ERROR");      
      },() => {
        this.initializeItems();
        this.loader.dismiss();
      });
  }

  private getPrices(rootModels, modelo) {
    //http://fipeapi.appspot.com/api/1/carros/veiculo/21/4828/2013-1.json
    let URL = this.API_DET_URL_OTHER+this.id_marca+"/"+modelo+"/"+this.id_year+".json";
    //console.log(URL);
    this.http.get(URL)
      .map((res) => res.json())
      .subscribe((data) => { 
         //console.log(data);
	 rootModels.preco = data.preco;
         rootModels.combustivel = data.combustivel;
         rootModels.ano_modelo = data.ano_modelo;
      }, err => {
        //this.loader.dismiss();
        console.log("ERROR");         
      },() => {
	console.log(this.rootModels);
        
      });
  }

  private initializeItems() {
    this.modelsList = this.rootModels;
  }


}
