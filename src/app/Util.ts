/**
 * Default
 * 
 * @author Nairan Omura<nairanomura@hotmail.com>
 * @since 28/10/2018
 */
export class Util {
    static API_DET_URL = 'http://fipeapi.appspot.com/api/1/carros/veiculo/';
    constructor() {}

    /**ionic ser
     * Método para organização de JSON por attributos
     * Ex.: let array = [{nome: ana, idade: 18}, ... ];
     * 
     *      result = Util.bucketSort(array, 'nome'); 
     * 
     * @author Nairan Omura<nairanomura@hotmail.com>
     * @since 28/10/2018
     * 
     * @param array Objeto JSON a ser organizado 
     * @param attribute Atributo índice da organização
     * 
     * @return JSON ordenado conforme atributo recebido
     */
    static bucketSort(array, attribute) {
        let bucket = new Array();

        /** BucketSort */
        for (let i = 0; i < array.length; i++) {
            let first = array[i][attribute]
                            .toLowerCase()
                            .charCodeAt();
            let check = false;

            bucket.forEach(function (ele, j) {
                if (j == first) {
                    bucket[first].unshift(array[i]);
                    check = true;
                    return false;
                }
            });
            if (!check) bucket[first] = [array[i]];
        }

        /** SelectionSort*/
        bucket.forEach(function (ele, i) {
            ele.forEach(function (item, j) {
                let menor = ele[j];
                ele.forEach(function (subitem, k) {
                    let valida = false, index = 0;
                    if (menor[attribute] == subitem[attribute]) return true;
                    while (!valida && menor[attribute].length > index++) {
                        let first = (menor[attribute]).charCodeAt(index);
                        let second = (subitem[attribute]).charCodeAt(index);

                        if (first > second || isNaN(second)) {
                            ele[j] = subitem;
                            ele[k] = menor;
                            menor = subitem;

                            valida = true;
                        } else if (first < second) {
                            valida = true;
                        }
                    }
                });
            });
        });

        let ArrayResult = new Array();
        bucket.forEach(function (item, i) {
            for (let j = item.length - 1; j >= 0; j--) {
                ArrayResult.push(item[j]);
            }
        });
        return ArrayResult;
    }

    static fileExists(url) {
        if (url){
            let req = new XMLHttpRequest();
            req.open('GET', url, false);
            req.send();
            return req.status==200;
        } else {
            return false;
        }
    }

    static sanitize(string) {

        if (string==undefined) return undefined;
        if (string=="") return "";
        
        string = string.replace("R$", "");
        string = string.replace(" ", "");
        string = string.replace(".", "");
        string = string.replace(",", "");

        return string;
    }
}
